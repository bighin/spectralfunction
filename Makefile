TARGET = spectral
LIBS = 
CC = gcc-mp-6
#CC = clang-mp-3.8
CFLAGS = -O2 -Wall -fopenmp -std=c11 -I/opt/local/include/
#CFLAGS = -g -pg
LDFLAGS = -lncurses -lgsl -L/opt/local/lib/

.PHONY: clean all default

default: $(TARGET)
all: default

OBJECTS = $(patsubst %.c, %.o, $(wildcard *.c libprogressbar/*.c cubature-1.0.2/hcubature.c))
HEADERS = $(wildcard *.h libprogressbar/*.h cubature/*.h)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(OBJECTS)
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJECTS) -Wall $(LIBS) -o $@

clean:
	-rm -f *.o libprogressbar/*.o cubature-1.0.2/*.o
	-rm -f $(TARGET)

#ifndef __QPW_H__
#define __QPW_H__

#include <math.h>
#include <complex.h>

double complex Tau1(short L,double complex omega,double n);
double complex Tau2(short L,double complex omega,double n);

double qp_pole(short L,double logn);
double qp_pole2(short L,double logn);

double zetaLM(short L,double logn,double omega);
double zetaLM_at_pole(short L,double logn);

double zeta2LM(short L,double logn,double omega);
double zeta2LM_at_pole(short L,double logn);

void beta2(short L,double logn,double omega,double *beta2s,short *lambdas,short *js,int *cnt,double *totalweight);
void beta2_at_pole(short L,double logn,double *beta2s,short *lambdas,short *js,int *cnt,double *totalweight);

#endif //__QPW_H__

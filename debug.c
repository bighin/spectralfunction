#include <stdio.h>
#include "selfenergies.h"

/*
	The following two functions are used to verify that the selection rules we find
	are correct. They can be compared with the rules found with Mathematica in the
	notebook Reference/SelectionRules.nb
*/

void debug_selection_rule(int L)
{
	int c,d;

	printf("mylistA%d={",L);

	for(c=0;c<iarules[L];c++)
	{
		struct selection_rule_t rule=arules[L][c];
	
		printf("{");
	
		for(d=1;d<=5;d++)
			printf("%d, ",rule.ls[d]);

		printf("%f}",rule.weight);
	
		if(c!=iarules[L]-1)
			printf(",\n");
	}

	printf("};\n\n");

	printf("mylistB%d={",L);

	for(c=0;c<ibrules[L];c++)
	{
		struct selection_rule_t rule=brules[L][c];
	
		printf("{");
	
		for(d=1;d<=5;d++)
		{
			printf("%d, ",rule.ls[d]);
		}

		printf("%f}",rule.weight);

		if(c!=ibrules[L]-1)
			printf(",\n");
	}

	printf("};\n\n");
}

int debug_selection_rules(void)
{
	int c;

	for(c=0;c<=SIGMA_HIGHEST_L_ORDER;c++)
		debug_selection_rule(c);

	return 0;
}

/*
	The following functions are used to debug the self-energies, they generate a list
	which can be compared with the results of the notebook Reference/SelfEnergies.nb
*/

void do_debug_sigmas(int L,double omega,double n)
{
	double complex s1,s2;
	double localepsilon=1e-4;

	s1=Sigma1(L,omega+I*localepsilon,n);
	s2=Sigma2(L,omega+I*localepsilon,n);

	printf("{%f%+fI, %f%+fI}",creal(s1),cimag(s1),creal(s2),cimag(s2));
}

void debug_sigmas(void)
{
	short L;

	printf("myResultsList={");

	for(L=0;L<=2;L++)
	{
		printf("{");
		
		do_debug_sigmas(L,1.1,2.2);
		printf(", ");

		do_debug_sigmas(L,2.2,3.3);
		printf(", ");

		do_debug_sigmas(L,0.2,6.4);
		printf("}");
		
		if(L!=2)
			printf(",\n");
	}
	
	printf("};\n");
}

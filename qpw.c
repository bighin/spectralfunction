#include <stdio.h>
#include <math.h>
#include <complex.h>

#include "selfenergies.h"

#ifndef M_PI
#define M_PI (3.14159265358979323846264338327950288)
#endif

/*
	We use the Cubature library for numerical integration,
	see http://ab-initio.mit.edu/wiki/index.php/Cubature
*/

#include "cubature-1.0.2/cubature.h"

/*
	Tuning paramaters for numerical integration, defined in spectral.c for convenience.
*/

extern size_t maxEval;
extern double relError;

/*
	This quantity appearing is really similar to the first-order self-energy, hence the name Tau.

	Differences: the denominator is squared, a global (-1) sign. Also here omega can be complex,
	and a small imaginary part has to be introduced manually.

	Essentially it is the derivative of the self-energy with respect to omega, useful in calculating
	the quasiparticle weight.

	Note: If we try to calculate the derivate numerically, rather than carrying it out analytically inside
	the integral, there are numerical problems.
*/

struct tau_ctx_t
{
	double complex omega;
	double n,cg2;

	short L,lambda,j;
};

int fTau1(unsigned ndim,const double *x,void *fdata,unsigned fdim,double *fval)
{
	/* The context from which we read the global variables */
	
	struct tau_ctx_t *ctx=(struct tau_ctx_t *)(fdata);

	/* Global variables */

	short lambda,j;
	double complex omega;
	double n,cg2;

	/* The integration variables */

	double t=x[0];
	double k=t/(1.0f-t);

	/* Auxiliary variables */
	
	double complex a,b,c,z;

	/* We load the global variables */

	omega=ctx->omega;
	n=ctx->n;
	cg2=ctx->cg2;

	lambda=ctx->lambda;
	j=ctx->j;

	/* Finally, we compute the result and return it */

#ifdef ALTERNATE

	/*
		The following is my code...
	*/

	a=(2.0f*lambda+1)/(4.0f*M_PI);
	b=pow(U(lambda,n,k),2.0f)*cg2;
	c=j*(j+1)-(omega)+omegak(k,n);

	z=a*b/(c*c)*pow(1-t,-2.0f);
#else
	/*
		and that's following Wojciech's notebook...
	*/

	a=sqrt((2.0f*lambda+1)/(4.0f*M_PI));
	b=U(lambda,n,k)*sqrt(cg2);
	c=j*(j+1)-(omega)+omegak(k,n);

	z=pow(creal(a*b/c),2.0f)*pow(1-t,-2.0f);

#endif
	
	fval[0]=creal(z);
	fval[1]=cimag(z);

	return 0;
}

double complex Tau1(short L,double complex omega,double n)
{
	double xmin[1]={0.0f};
	double xmax[1]={1.0f};
	double res[2],err[2];

	double complex total;

	struct tau_ctx_t ctx;

	ctx.L=L;
	ctx.n=n;
	ctx.omega=omega;

	total=0.0f;

	/*
		The selection rules from L=0 to L=3, along with the Clebsch-Gordan weights
		are calculated in Mathematica and hardcoded here... It could be improved...
	*/

	switch(L)
	{
		case 0:
		
		ctx.lambda=0;
		ctx.j=0;
		ctx.cg2=1.0f;

		hcubature(2,fTau1,&ctx,1,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
		total+=res[0]+I*res[1];
		
		ctx.lambda=1;
		ctx.j=1;
		ctx.cg2=1.0f;

		hcubature(2,fTau1,&ctx,1,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
		total+=res[0]+I*res[1];

		break;

		case 1:
		
		ctx.lambda=0;
		ctx.j=1;
		ctx.cg2=1.0f;

		hcubature(2,fTau1,&ctx,1,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
		total+=res[0]+I*res[1];

		ctx.lambda=1;
		ctx.j=0;
		ctx.cg2=1.0f/3.0f;

		hcubature(2,fTau1,&ctx,1,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
		total+=res[0]+I*res[1];

		ctx.lambda=1;
		ctx.j=2;
		ctx.cg2=2.0f/3.0f;

		hcubature(2,fTau1,&ctx,1,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
		total+=res[0]+I*res[1];

		break;

		case 2:
		
		ctx.lambda=0;
		ctx.j=2;
		ctx.cg2=1.0f;

		hcubature(2,fTau1,&ctx,1,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
		total+=res[0]+I*res[1];

		ctx.lambda=1;
		ctx.j=1;
		ctx.cg2=2.0f/5.0f;

		hcubature(2,fTau1,&ctx,1,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
		total+=res[0]+I*res[1];

		ctx.lambda=1;
		ctx.j=3;
		ctx.cg2=3.0f/5.0f;

		hcubature(2,fTau1,&ctx,1,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
		total+=res[0]+I*res[1];

		break;

		case 3:
		
		ctx.lambda=0;
		ctx.j=3;
		ctx.cg2=1.0f;

		hcubature(2,fTau1,&ctx,1,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
		total+=res[0]+I*res[1];

		ctx.lambda=1;
		ctx.j=2;
		ctx.cg2=3.0f/7.0f;

		hcubature(2,fTau1,&ctx,1,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
		total+=res[0]+I*res[1];

		ctx.lambda=1;
		ctx.j=4;
		ctx.cg2=4.0f/7.0f;

		hcubature(2,fTau1,&ctx,1,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
		total+=res[0]+I*res[1];

		break;
		
		default:

		fprintf(stderr,"Not implemented! Please, implement me... :)\n");
		exit(0);
	}

	return total;
}

/*
	Exactly the same thing, for the second order
*/

struct tau2_ctx_t
{
	double n;
	double complex omega;

	short L,l1,l2,l3,l4,l5;
};

int fTau2A(unsigned ndim,const double *x,void *fdata,unsigned fdim,double *fval)
{
	/* The context from which we read the global variables */

	struct tau2_ctx_t *ctx=(struct tau2_ctx_t *)(fdata);

	/* Global variables */

	short l1,l2,l3,l4,l5;
	double n;
	double complex omega;

	/* The integration variables */

	double t1=x[0];
	double k1=t1/(1.0f-t1);

	double t2=x[1];
	double k2=t2/(1.0f-t2);

	/* Auxiliary variables */
	
	double complex a,b,c,d,f,z,El2,El4,El5;

	/* We load the global variables */

	omega=ctx->omega;
	n=ctx->n;

	l1=ctx->l1;
	l2=ctx->l2;
	l3=ctx->l3;
	l4=ctx->l4;
	l5=ctx->l5;

	/* Finally, we compute the result and return it */

	El2=l2*(l2+1);
	El4=l4*(l4+1);
	El5=l5*(l5+1);

	a=pow(U(l1,n,k1),2.0f);
	b=pow(U(l3,n,k2),2.0f);
	c=omega-omegak(k1,n)-El2;
	d=omega-omegak(k2,n)-El5;
	f=-omega+El4+omegak(k1,n)+omegak(k2,n);

	z=a*b/(c*d*f*f);
	z-=a*b/(c*d*d*f);
	z-=a*b/(c*c*d*f);
	
	z*=pow(1-t1,-2.0f)*pow(1-t2,-2.0f);

	fval[0]=creal(z);
	fval[1]=cimag(z);

	return 0;
}

int fTau2B(unsigned ndim,const double *x,void *fdata,unsigned fdim,double *fval)
{
	/* The context from which we read the global variables */
	
	struct tau2_ctx_t *ctx=(struct tau2_ctx_t *)(fdata);

	/* Global variables */

	short l1,l2,l3,l4,l5;
	double n;
	double complex omega;

	/* The integration variables */

	double t1=x[0];
	double k1=t1/(1.0f-t1);

	double t2=x[1];
	double k2=t2/(1.0f-t2);

	/* Auxiliary variables */
	
	double complex a,b,c,d,f,z,El2,El4,El5;

	/* We load the global variables */

	omega=ctx->omega;
	n=ctx->n;

	l1=ctx->l1;
	l2=ctx->l2;
	l3=ctx->l3;
	l4=ctx->l4;
	l5=ctx->l5;

	/* Finally, we compute the result and return it */

	El2=l2*(l2+1);
	El4=l4*(l4+1);
	El5=l5*(l5+1);

	a=pow(U(l1,n,k1),2.0f);
	b=pow(U(l3,n,k2),2.0f);
	c=omega-omegak(k1,n)-El2;
	d=omega-omegak(k1,n)-El5;
	f=-omega+El4+omegak(k1,n)+omegak(k2,n);

	z=a*b/(c*d*f*f);
	z-=a*b/(c*d*d*f);
	z-=a*b/(c*c*d*f);
	
	z*=pow(1-t1,-2.0f)*pow(1-t2,-2.0f);

	fval[0]=creal(z);
	fval[1]=cimag(z);

	return 0;
}

/*
	Selection rules for 2nd order diagrams
*/

double complex Tau2_from_selection_rule(struct selection_rule_t rule,char type,double complex omega,double n)
{
	double xmin[2]={0.0f,0.0f};
	double xmax[2]={1.0f,1.0f};
	double res[2],err[2];

	double complex total;

	struct tau2_ctx_t ctx;

	ctx.L=rule.ls[0];
	ctx.n=n;
	ctx.omega=omega;

	ctx.l1=rule.ls[1];
	ctx.l2=rule.ls[2];
	ctx.l3=rule.ls[3];
	ctx.l4=rule.ls[4];
	ctx.l5=rule.ls[5];

	total=0.0f;

	switch(type)
	{
		case 'A':
		
		hcubature(2,fTau2A,&ctx,2,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
		total+=res[0]+I*res[1];

		break;
		
		case 'B':

		hcubature(2,fTau2B,&ctx,2,xmin,xmax,maxEval,0,relError,ERROR_INDIVIDUAL,res,err);
		total+=res[0]+I*res[1];

		break;
		
		default:
		
		fprintf(stderr,"Wrong 2nd-order diagram type!\n");
		exit(0);
	}

	return rule.weight*total;
}

extern struct selection_rule_t *arules[SIGMA_HIGHEST_L_ORDER+1],*brules[SIGMA_HIGHEST_L_ORDER+1];
extern short iarules[SIGMA_HIGHEST_L_ORDER+1],ibrules[SIGMA_HIGHEST_L_ORDER+1];

#define TAU_HIGHEST_L_ORDER	(SIGMA_HIGHEST_L_ORDER)

double complex Tau2(short L,double complex omega,double n)
{
	double complex res=0.0f;
	int c;

	if(L>TAU_HIGHEST_L_ORDER)
	{
		fprintf(stderr,"Tau order too high! (L=%d, Lmax=%d)\n",L,TAU_HIGHEST_L_ORDER);
		exit(0);
	}

	for(c=0;c<iarules[L];c++)
		res+=Tau2_from_selection_rule(arules[L][c],'A',omega,n);

	for(c=0;c<ibrules[L];c++)
		res+=Tau2_from_selection_rule(brules[L][c],'B',omega,n);
	
	return res;
}


/*
	The Clebsch-Gordan coefficients, given the Wigner 3j symbols.
*/

double wigner3j(int l1,int l2,int l3,int m1,int m2,int m3);

double cg(short L,short M,short l1,short m1,short l2,short m2)
{
	short phase=l1-l2+M;
	
	return pow(-1.0f,phase)*sqrt(2*L+1)*wigner3j(l1,l2,L,m1,m2,-M);
}

/*
	Find the location of the quasiparticle pole, for a given L
*/

double qp_pole(short L,double logn)
{
        double omegalo,omegahi,omegamid;
        double elo,ehi,emid;

        short c,maxiter=32;

	/*
		A simple bracketing rootfinding algorithm.
	*/

        omegalo=-64.0f;
        omegahi=L*(L+1);

        elo=omegalo-L*(L+1)-creal(Sigma1(L,omegalo,exp(logn)));
        ehi=omegahi-L*(L+1)-creal(Sigma1(L,omegahi,exp(logn)));

        if((elo*ehi)>=0.0f)
        {
                fprintf(stderr,"Initial values do not bracket a root (elo=%f, ehi=%f).\n",elo,ehi);
                return -1;
        }

        for(c=0;c<maxiter;c++)
        {
                omegamid=(omegahi+omegalo)/2.0f;
                emid=omegamid-L*(L+1)-creal(Sigma1(L,omegamid,exp(logn)));

                if(emid==0.0f)
                        break;

                if((emid*elo)<=0.0f)
                {
                        omegahi=omegamid;
                        ehi=emid;
                }

                else if((emid*ehi)<=0.0f)
                {
                        omegalo=omegamid;
                        elo=emid;
                }

                else
                {
                        fprintf(stderr,"Error in rootfinding algorithm.\n");
                        return -1;
                }
        }

	return omegamid;
}

/*
	Find the location of the quasiparticle pole, for a given L, at second order.
*/

double qp_pole2(short L,double logn)
{
        double omegalo,omegahi,omegamid;
        double elo,ehi,emid;

        short c,maxiter=32;

	/*
		A simple bracketing rootfinding algorithm.
	*/

        omegalo=-64.0f;
        omegahi=L*(L+1);

        elo=omegalo-L*(L+1)-creal(Sigma1(L,omegalo,exp(logn))+Sigma2(L,omegalo,exp(logn)));
        ehi=omegahi-L*(L+1)-creal(Sigma1(L,omegahi,exp(logn))+Sigma2(L,omegahi,exp(logn)));

        if((elo*ehi)>=0.0f)
        {
                fprintf(stderr,"Initial values do not bracket a root (elo=%f, ehi=%f).\n",elo,ehi);
                return -1;
        }

        for(c=0;c<maxiter;c++)
        {
                omegamid=(omegahi+omegalo)/2.0f;
                emid=omegamid-L*(L+1)-creal(Sigma1(L,omegamid,exp(logn))+Sigma2(L,omegamid,exp(logn)));

                if(emid==0.0f)
                        break;

                if((emid*elo)<=0.0f)
                {
                        omegahi=omegamid;
                        ehi=emid;
                }

                else if((emid*ehi)<=0.0f)
                {
                        omegalo=omegamid;
                        elo=emid;
                }

                else
                {
                        fprintf(stderr,"Error in rootfinding algorithm.\n");
                        return -1;
                }
        }

	return omegamid;
}

/*
	The epsilon to be used in the quasiparticle formula, to be set in spectral.c
*/

extern double qpw_epsilon;

/*
	Quasiparticle weight, for a given L, density and energy.
*/

double zetaLM(short L,double logn,double omega)
{
	return 1.0f/(1.0f+creal(Tau1(L,omega+qpw_epsilon*I,exp(logn))));
}

double zetaLM_at_pole(short L,double logn)
{
	return 	zetaLM(L,logn,qp_pole(L,logn));
}

/*
	Quasiparticle weight, for a given L, density and energy, second order.
*/

double zeta2LM(short L,double logn,double omega)
{
	return 1.0f/(1.0f+creal(Tau1(L,omega+qpw_epsilon*I,exp(logn))+Tau2(L,omega+qpw_epsilon*I,exp(logn))));
}

double zeta2LM_at_pole(short L,double logn)
{
	return 	zeta2LM(L,logn,qp_pole2(L,logn));
}

/*
	The betas, squared.

	Input parameters: L, logn and omega (the energy). 

	Output: double *beta2s, an array with the calculated values of |beta|^2 (phonon populations)
                short *lambdas, an array with the corresponding value for lambda (subscript of beta)
		short *js, an array with the corresponding value for j (subscript of beta)
		int *cnt, a point to an int containing the number of betas
		double *totalweight, should be 1.0f

	Each array should be big enough as to contain all the values that will be produced.

	The values of lambda and j depend only on the selection rules, and it is guaranteed
	that they will be in the same order for equal Ls.
*/

void beta2(short L,double logn,double omega,double *beta2s,short *lambdas,short *js,int *cnt,double *totalweight)
{
	double zeta;
	short lambda,j;
	
	zeta=zetaLM(L,logn,omega);
	
	*cnt=0;
	*totalweight=zeta;

	for(lambda=0;lambda<=1;lambda++)
	{
		for(j=0;j<=7;j++)
		{
			double beta2=0.0f;
			double k,x,y;
			double cg0;

			cg0=cg(j,0,L,0,lambda,0);

			if(fabs(cg0)<10e-4)
				continue;

			lambdas[*cnt]=lambda;
			js[*cnt]=j;

			x=pow(-1.0f,lambda+1)*sqrt((2.0f*lambda+1.0f)/(4*M_PI))*cg0*sqrt(zeta);

			/*
				Poor man's integration over k
			
				ToDO: improve it using hcubature.
			*/

			for(k=0.01f;k<=100.0f;k+=0.001f)
			{
				y=x*U(lambda,exp(logn),k)*creal(1.0f/(j*(j+1)-omega-I*0.01+omegak(k,exp(logn))));

				beta2+=0.001*pow(creal(y),2.0f);
			}
		
			beta2s[*cnt]=beta2;
			*cnt=*cnt+1;

			*totalweight+=beta2;
		}
	}
}

void beta2_at_pole(short L,double logn,double *beta2s,short *lambdas,short *js,int *cnt,double *totalweight)
{
	beta2(L,logn,qp_pole(L,logn),beta2s,lambdas,js,cnt,totalweight);
}

#ifndef __SPECTRAL_DEBUG_H__
#define __SPECTRAL_DEBUG_H__

int debug_selection_rules(void);
void debug_sigmas(void);

#endif //__SPECTRAL_DEBUG_H__

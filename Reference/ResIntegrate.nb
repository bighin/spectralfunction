(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.2' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     29682,        874]
NotebookOptionsPosition[     27373,        795]
NotebookOutlinePosition[     27728,        811]
CellTagsIndexPosition[     27685,        808]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"G0", "[", 
   RowBox[{"\[Omega]_", ",", "n_"}], "]"}], ":=", 
  FractionBox["1", 
   RowBox[{"\[Omega]", "-", 
    RowBox[{"el", "[", "n", "]"}], "+", 
    RowBox[{"I", "*", "\[Delta]"}]}]]}]], "Input",
 CellChangeTimes->{{3.690874884273602*^9, 3.690874904342779*^9}, 
   3.691346350255477*^9, 3.691769080703483*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[Chi]", "[", 
   RowBox[{"\[Omega]_", ",", "n_"}], "]"}], ":=", 
  FractionBox["1", 
   RowBox[{"\[Omega]", "-", 
    RowBox[{"\[Omega]k", "[", "n", "]"}], "+", 
    RowBox[{"I", "*", "\[Delta]"}]}]]}]], "Input",
 CellChangeTimes->{{3.690874909694016*^9, 3.6908749224574203`*^9}, 
   3.691346354558363*^9, 3.691769084243627*^9}],

Cell["The function to integrate from \[Omega]1=-\[Infinity] to \[Omega]1=+\
\[Infinity]", "Text",
 CellChangeTimes->{{3.6908746127555113`*^9, 3.690874631528648*^9}, {
  3.690874967379993*^9, 3.6908749695719852`*^9}, {3.691769086076256*^9, 
  3.691769086459421*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"f", "=", 
   RowBox[{
    FractionBox["1", 
     RowBox[{"2", "*", "\[Pi]"}]], 
    RowBox[{"G0", "[", 
     RowBox[{
      RowBox[{"\[Omega]", "-", "\[Omega]1"}], ",", "1"}], "]"}], "*", 
    RowBox[{"\[Chi]", "[", 
     RowBox[{"\[Omega]1", ",", "2"}], "]"}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.690873226648288*^9, 3.6908732320590687`*^9}, {
   3.6908732631106453`*^9, 3.690873280751091*^9}, 3.690873347931197*^9, {
   3.6908740682291737`*^9, 3.690874172102778*^9}, {3.690874209258498*^9, 
   3.690874226672762*^9}, 3.6908744306941547`*^9, 3.6908744857912283`*^9, {
   3.6908747574728117`*^9, 3.690874758779615*^9}, {3.690874846048521*^9, 
   3.690874846703023*^9}, {3.690874929721444*^9, 3.690874944189616*^9}, {
   3.6908749755561743`*^9, 3.690874996739813*^9}}],

Cell["\<\
Let\[CloseCurlyQuote]s find the poles of the function, with respect to the \
integration variable \[Omega]1\
\>", "Text",
 CellChangeTimes->{{3.6908746481580877`*^9, 3.69087465621943*^9}, {
  3.690874955725603*^9, 3.690874965636265*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"poles", "=", 
   RowBox[{"Solve", "[", 
    RowBox[{
     RowBox[{
      SuperscriptBox["f", 
       RowBox[{"-", "1"}]], "\[Equal]", "0"}], ",", "\[Omega]1"}], "]"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.690874709781148*^9, 3.690874715149506*^9}, 
   3.690874982636352*^9}],

Cell["\<\
Closing the contour on the upper half plane, we select only the poles in the \
upper half plane (i.e. with Im z > 0)\
\>", "Text",
 CellChangeTimes->{{3.690874658789003*^9, 3.690874700021221*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"ImaginaryPartSign", "[", "expr_", "]"}], ":=", 
  RowBox[{"Sign", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"CoefficientList", "[", 
      RowBox[{
       RowBox[{"Series", "[", 
        RowBox[{"expr", ",", 
         RowBox[{"{", 
          RowBox[{"\[Delta]", ",", "0", ",", "1"}], "}"}]}], "]"}], ",", 
       "\[Delta]"}], "]"}], "[", 
     RowBox[{"[", "2", "]"}], "]"}], "/", "I"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.6908742995568047`*^9, 3.6908744033927813`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"upperHalfPlanePoles", "=", 
   RowBox[{
    RowBox[{
     RowBox[{"#", "[", 
      RowBox[{"[", "1", "]"}], "]"}], "&"}], "/@", 
    RowBox[{"Select", "[", 
     RowBox[{
      RowBox[{
       RowBox[{
        RowBox[{"{", 
         RowBox[{"#", ",", 
          RowBox[{"ImaginaryPartSign", "[", 
           RowBox[{"\[Omega]1", "/.", "#"}], "]"}]}], "}"}], "&"}], "/@", 
       "poles"}], ",", 
      RowBox[{
       RowBox[{
        RowBox[{"#", "[", 
         RowBox[{"[", "2", "]"}], "]"}], ">", "0"}], "&"}]}], "]"}]}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.690874157048889*^9, 3.6908741617738733`*^9}, {
   3.6908741943983917`*^9, 3.690874247992243*^9}, {3.690874280781541*^9, 
   3.6908742838137913`*^9}, {3.690874396097138*^9, 3.690874516718812*^9}, 
   3.690874646711179*^9, {3.690874719740835*^9, 3.6908747246370487`*^9}, {
   3.6908750334464283`*^9, 3.6908750661366034`*^9}, {3.690875155641253*^9, 
   3.690875189490583*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Simplify", "[", 
  RowBox[{"2", "*", "\[Pi]", "*", "I", "*", 
   RowBox[{"Total", "[", 
    RowBox[{
     RowBox[{
      RowBox[{
       RowBox[{"Residue", "[", 
        RowBox[{"f", ",", 
         RowBox[{"{", 
          RowBox[{"\[Omega]1", ",", 
           RowBox[{"\[Omega]1", "/.", "#"}]}], "}"}]}], "]"}], "&"}], "/@", 
      "upperHalfPlanePoles"}], "/.", 
     RowBox[{"{", 
      RowBox[{"\[Delta]", "\[Rule]", "0"}], "}"}]}], "]"}]}], "]"}]], "Input",\

 CellChangeTimes->{{3.690874568305155*^9, 3.690874597018762*^9}, {
  3.69087472954867*^9, 3.6908747656463547`*^9}, {3.690874816631565*^9, 
  3.690874817798237*^9}, {3.690874858094782*^9, 3.690874860638508*^9}, {
  3.690875076184464*^9, 3.690875076928307*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox["\[ImaginaryI]", 
   RowBox[{"\[Omega]", "-", 
    RowBox[{"el", "[", "1", "]"}], "-", 
    RowBox[{"\[Omega]k", "[", "2", "]"}]}]]}]], "Output",
 CellChangeTimes->{{3.690874598209779*^9, 3.690874607371008*^9}, {
   3.690874730374856*^9, 3.690874771760874*^9}, 3.6908748180861397`*^9, {
   3.690874852184804*^9, 3.690874860924706*^9}, 3.690875009554093*^9, {
   3.6908750687048388`*^9, 3.69087507811235*^9}, 3.69087519177017*^9, 
   3.691344608312689*^9, 3.691769095411869*^9, 3.691927162725666*^9, 
   3.6919290551349907`*^9, 3.6919405088561*^9}]
}, Open  ]],

Cell["Let\[CloseCurlyQuote]s try to do everything in one single function", \
"Text",
 CellChangeTimes->{{3.690875209893764*^9, 3.690875221832218*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"ResIntegrate", "[", 
   RowBox[{"f_", ",", "\[Omega]n_"}], "]"}], ":=", 
  RowBox[{"With", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"poles", "=", 
      RowBox[{"Solve", "[", 
       RowBox[{
        RowBox[{
         SuperscriptBox["f", 
          RowBox[{"-", "1"}]], "\[Equal]", "0"}], ",", "\[Omega]n"}], "]"}]}],
      "}"}], ",", 
    RowBox[{"With", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"upperHalfPlanePoles", "=", 
        RowBox[{
         RowBox[{
          RowBox[{"#", "[", 
           RowBox[{"[", "1", "]"}], "]"}], "&"}], "/@", 
         RowBox[{"Select", "[", 
          RowBox[{
           RowBox[{
            RowBox[{
             RowBox[{"{", 
              RowBox[{"#", ",", 
               RowBox[{"ImaginaryPartSign", "[", 
                RowBox[{"\[Omega]n", "/.", "#"}], "]"}]}], "}"}], "&"}], "/@",
             "poles"}], ",", 
           RowBox[{
            RowBox[{
             RowBox[{"#", "[", 
              RowBox[{"[", "2", "]"}], "]"}], ">", "0"}], "&"}]}], "]"}]}]}], 
       "}"}], ",", 
      RowBox[{"Simplify", "[", 
       RowBox[{"2", "*", "\[Pi]", "*", "I", "*", 
        RowBox[{"Total", "[", 
         RowBox[{
          RowBox[{
           RowBox[{"Residue", "[", 
            RowBox[{"f", ",", 
             RowBox[{"{", 
              RowBox[{"\[Omega]n", ",", 
               RowBox[{"\[Omega]n", "/.", "#"}]}], "}"}]}], "]"}], "&"}], "/@",
           "upperHalfPlanePoles"}], "]"}]}], "]"}]}], "]"}]}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.690875230755712*^9, 3.690875289746009*^9}, {
  3.6908753480920467`*^9, 3.690875366040351*^9}, {3.690875533305852*^9, 
  3.690875534593314*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ResIntegrate", "[", 
   RowBox[{
    RowBox[{
     FractionBox["1", 
      RowBox[{"2", "*", "\[Pi]"}]], 
     RowBox[{"G0", "[", 
      RowBox[{
       RowBox[{"\[Omega]", "-", "\[Omega]1"}], ",", "1"}], "]"}], "*", 
     RowBox[{"\[Chi]", "[", 
      RowBox[{"\[Omega]1", ",", "2"}], "]"}]}], ",", "\[Omega]1"}], "]"}], "/.", 
  RowBox[{"{", 
   RowBox[{"\[Delta]", "\[Rule]", "0"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.690875295838934*^9, 3.6908753134883223`*^9}, {
  3.691769135344409*^9, 3.691769138815296*^9}}],

Cell[BoxData[
 RowBox[{"-", 
  FractionBox["\[ImaginaryI]", 
   RowBox[{"\[Omega]", "-", 
    RowBox[{"el", "[", "1", "]"}], "-", 
    RowBox[{"\[Omega]k", "[", "2", "]"}]}]]}]], "Output",
 CellChangeTimes->{{3.690875309996221*^9, 3.690875313864827*^9}, {
   3.6908753497365713`*^9, 3.6908753680917788`*^9}, 3.69134460838935*^9, {
   3.691769115686515*^9, 3.691769139095072*^9}, 3.691927167440856*^9, 
   3.6919290551989202`*^9, 3.69194050890294*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    FractionBox["1", 
     RowBox[{"2", "*", "\[Pi]"}]], 
    RowBox[{"ResIntegrate", "[", 
     RowBox[{
      RowBox[{
       FractionBox["1", 
        RowBox[{"2", "*", "\[Pi]"}]], 
       RowBox[{"ResIntegrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"G0", "[", 
           RowBox[{
            RowBox[{"\[Omega]", "-", "\[Omega]1"}], ",", "2"}], "]"}], "*", 
          RowBox[{"G0", "[", 
           RowBox[{
            RowBox[{"\[Omega]", "-", "\[Omega]1", "-", "\[Omega]2"}], ",", 
            "4"}], "]"}], "*", 
          RowBox[{"G0", "[", 
           RowBox[{
            RowBox[{"\[Omega]", "-", "\[Omega]2"}], ",", "5"}], "]"}], "*", 
          RowBox[{"\[Chi]", "[", 
           RowBox[{"\[Omega]1", ",", "1"}], "]"}], "*", 
          RowBox[{"\[Chi]", "[", 
           RowBox[{"\[Omega]2", ",", "3"}], "]"}]}], ",", "\[Omega]1"}], 
        "]"}]}], ",", "\[Omega]2"}], "]"}]}], "/.", 
   RowBox[{"{", 
    RowBox[{"\[Delta]", "\[Rule]", "0"}], "}"}]}], "//", 
  "Simplify"}]], "Input",
 CellChangeTimes->{{3.6908754040252123`*^9, 3.690875549264337*^9}, {
  3.6908756743736343`*^9, 3.690875706475404*^9}}],

Cell[BoxData[
 FractionBox["1", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"\[Omega]", "-", 
     RowBox[{"el", "[", "2", "]"}], "-", 
     RowBox[{"\[Omega]k", "[", "1", "]"}]}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{"\[Omega]", "-", 
     RowBox[{"el", "[", "5", "]"}], "-", 
     RowBox[{"\[Omega]k", "[", "3", "]"}]}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "\[Omega]"}], "+", 
     RowBox[{"el", "[", "4", "]"}], "+", 
     RowBox[{"\[Omega]k", "[", "1", "]"}], "+", 
     RowBox[{"\[Omega]k", "[", "3", "]"}]}], ")"}]}]]], "Output",
 CellChangeTimes->{{3.69087567219993*^9, 3.69087570682329*^9}, 
   3.691344608621222*^9, 3.69134661252346*^9, 3.6917691222395887`*^9, 
   3.691927173922402*^9, 3.691929055434742*^9, 3.691940509100561*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    FractionBox["1", 
     RowBox[{"2", "*", "\[Pi]"}]], 
    RowBox[{"ResIntegrate", "[", 
     RowBox[{
      RowBox[{
       FractionBox["1", 
        RowBox[{"2", "*", "\[Pi]"}]], 
       RowBox[{"ResIntegrate", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"G0", "[", 
           RowBox[{
            RowBox[{"\[Omega]", "-", "\[Omega]1"}], ",", "2"}], "]"}], "*", 
          RowBox[{"G0", "[", 
           RowBox[{
            RowBox[{"\[Omega]", "-", "\[Omega]1", "-", "\[Omega]2"}], ",", 
            "4"}], "]"}], "*", 
          RowBox[{"G0", "[", 
           RowBox[{
            RowBox[{"\[Omega]", "-", "\[Omega]1"}], ",", "5"}], "]"}], "*", 
          RowBox[{"\[Chi]", "[", 
           RowBox[{"\[Omega]1", ",", "1"}], "]"}], "*", 
          RowBox[{"\[Chi]", "[", 
           RowBox[{"\[Omega]2", ",", "3"}], "]"}]}], ",", "\[Omega]1"}], 
        "]"}]}], ",", "\[Omega]2"}], "]"}]}], "/.", 
   RowBox[{"{", 
    RowBox[{"\[Delta]", "\[Rule]", "0"}], "}"}]}], "//", 
  "Simplify"}]], "Input",
 CellChangeTimes->{{3.691344916199871*^9, 3.691344954999209*^9}}],

Cell[BoxData[
 FractionBox["1", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"\[Omega]", "-", 
     RowBox[{"el", "[", "2", "]"}], "-", 
     RowBox[{"\[Omega]k", "[", "1", "]"}]}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{"\[Omega]", "-", 
     RowBox[{"el", "[", "5", "]"}], "-", 
     RowBox[{"\[Omega]k", "[", "1", "]"}]}], ")"}], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"-", "\[Omega]"}], "+", 
     RowBox[{"el", "[", "4", "]"}], "+", 
     RowBox[{"\[Omega]k", "[", "1", "]"}], "+", 
     RowBox[{"\[Omega]k", "[", "3", "]"}]}], ")"}]}]]], "Output",
 CellChangeTimes->{
  3.691344781132352*^9, {3.691344917151477*^9, 3.691344956610731*^9}, 
   3.691346359569998*^9, 3.6913466142024803`*^9, 3.691769124896041*^9, 
   3.69192717571959*^9, 3.691929055664033*^9, 3.69194050928131*^9}]
}, Open  ]],

Cell["Test \[CapitalSigma]1:", "Text",
 CellChangeTimes->{{3.691927620200213*^9, 3.691927628466298*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"f", "=", 
   RowBox[{
    FractionBox["1", 
     RowBox[{"2", "*", "\[Pi]"}]], 
    RowBox[{"G0", "[", 
     RowBox[{
      RowBox[{"\[Omega]", "-", "\[Omega]1"}], ",", "1"}], "]"}], "*", 
    RowBox[{"\[Chi]", "[", 
     RowBox[{"\[Omega]1", ",", "2"}], "]"}]}]}], ";"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"sublist", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"\[Omega]", "\[Rule]", 
      RowBox[{"1.1", "+", 
       RowBox[{"I", "*", "0.8"}]}]}], ",", 
     RowBox[{
      RowBox[{"el", "[", "1", "]"}], "\[Rule]", "2.4"}], ",", 
     RowBox[{
      RowBox[{"\[Omega]k", "[", "2", "]"}], "\[Rule]", "3.7"}], ",", 
     RowBox[{"\[Delta]", "\[Rule]", "0.8"}]}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.69192763949951*^9, 3.691927693844553*^9}, {
  3.6919277638676863`*^9, 3.6919277658186283`*^9}, {3.691927818064911*^9, 
  3.691927818816264*^9}, {3.691929074405154*^9, 3.6919290896063538`*^9}, {
  3.691929121722555*^9, 3.691929196439817*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NIntegrate", "[", 
  RowBox[{
   RowBox[{"f", "/.", "sublist"}], ",", 
   RowBox[{"{", 
    RowBox[{"\[Omega]1", ",", 
     RowBox[{"-", "Infinity"}], ",", "Infinity"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.691927672850902*^9, 3.691927710701482*^9}, {
   3.691928573611251*^9, 3.691928581721936*^9}, 3.691929097991641*^9, 
   3.691929133236534*^9, {3.691929176154787*^9, 3.6919292030810413`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "0.07802340702222624`"}], "+", 
  RowBox[{"0.1625487646294168`", " ", "\[ImaginaryI]"}]}]], "Output",
 CellChangeTimes->{{3.691927681079359*^9, 3.6919277110124397`*^9}, 
   3.691927767402248*^9, 3.6919278203180237`*^9, {3.691928574784295*^9, 
   3.6919285820001793`*^9}, {3.691929055877603*^9, 3.69192909103512*^9}, {
   3.691929126889426*^9, 3.691929203308794*^9}, 3.6919405093737164`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ResIntegrate", "[", 
   RowBox[{"f", ",", "\[Omega]1"}], "]"}], "/.", "sublist"}]], "Input",
 CellChangeTimes->{{3.691927712195312*^9, 3.69192771994145*^9}, {
  3.69192778504552*^9, 3.691927828743999*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "0.07802340702210664`"}], "+", 
  RowBox[{"0.1625487646293888`", " ", "\[ImaginaryI]"}]}]], "Output",
 CellChangeTimes->{{3.6919278013766003`*^9, 3.691927829240511*^9}, 
   3.691929055920602*^9, 3.691929208643909*^9, 3.691940509402184*^9}]
}, Open  ]],

Cell["Test A:", "Text",
 CellChangeTimes->{{3.691927514764608*^9, 3.691927517015362*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"sublist", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"\[Omega]", "\[Rule]", 
      RowBox[{"1.1", "+", 
       RowBox[{"I", "*", "0.8"}]}]}], ",", 
     RowBox[{
      RowBox[{"el", "[", "2", "]"}], "\[Rule]", "2.2"}], ",", 
     RowBox[{
      RowBox[{"el", "[", "4", "]"}], "\[Rule]", "4.4"}], ",", 
     RowBox[{
      RowBox[{"el", "[", "5", "]"}], "\[Rule]", "5.5"}], ",", 
     RowBox[{
      RowBox[{"\[Omega]k", "[", "1", "]"}], "\[Rule]", "1.1"}], ",", 
     RowBox[{
      RowBox[{"\[Omega]k", "[", "3", "]"}], "\[Rule]", "3.3"}], ",", 
     RowBox[{"\[Delta]", "\[Rule]", "0.8"}]}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.691927397222089*^9, 3.691927452253333*^9}, {
  3.691927491812808*^9, 3.691927518806861*^9}, {3.691929221303973*^9, 
  3.69192922668655*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"f", "=", 
   RowBox[{
    FractionBox["1", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"2", "*", "\[Pi]"}], ")"}], "2"]], 
    RowBox[{"G0", "[", 
     RowBox[{
      RowBox[{"\[Omega]", "-", "\[Omega]1"}], ",", "2"}], "]"}], "*", 
    RowBox[{"G0", "[", 
     RowBox[{
      RowBox[{"\[Omega]", "-", "\[Omega]1", "-", "\[Omega]2"}], ",", "4"}], 
     "]"}], "*", 
    RowBox[{"G0", "[", 
     RowBox[{
      RowBox[{"\[Omega]", "-", "\[Omega]2"}], ",", "5"}], "]"}], "*", 
    RowBox[{"\[Chi]", "[", 
     RowBox[{"\[Omega]1", ",", "1"}], "]"}], "*", 
    RowBox[{"\[Chi]", "[", 
     RowBox[{"\[Omega]2", ",", "3"}], "]"}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.691927519787834*^9, 3.691927521479697*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NIntegrate", "[", 
  RowBox[{
   RowBox[{"f", "/.", "sublist"}], ",", 
   RowBox[{"{", 
    RowBox[{"\[Omega]1", ",", 
     RowBox[{"-", "Infinity"}], ",", "Infinity"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\[Omega]2", ",", 
     RowBox[{"-", "Infinity"}], ",", "Infinity"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.6919273500777903`*^9, 3.691927393384145*^9}, {
   3.6919274596296577`*^9, 3.6919274654852123`*^9}, 3.691927513377625*^9, 
   3.691927545704673*^9, 3.691929239062677*^9}],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"NIntegrate", "::", "slwcon"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"Numerical integration converging too slowly; suspect one \
of the following: singularity, value of the integration is 0, highly \
oscillatory integrand, or WorkingPrecision too small. \\!\\(\\*ButtonBox[\\\"\
\[RightSkeleton]\\\", ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/message/NIntegrate/slwcon\\\", ButtonNote -> \
\\\"NIntegrate::slwcon\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{
  3.691929056007248*^9, {3.691929236133819*^9, 3.691929239396109*^9}, 
   3.691940509543036*^9}],

Cell[BoxData[
 RowBox[{"0.0002097072752695649`", "\[VeryThinSpace]", "+", 
  RowBox[{"0.004562249046417872`", " ", "\[ImaginaryI]"}]}]], "Output",
 CellChangeTimes->{
  3.691927363618849*^9, 3.691927393756221*^9, 3.6919274657785378`*^9, 
   3.6919275000046597`*^9, 3.691927546356277*^9, 3.691929056125355*^9, {
   3.691929237237672*^9, 3.6919292405006113`*^9}, 3.691940510561743*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"ResIntegrate", "[", 
    RowBox[{
     RowBox[{"ResIntegrate", "[", 
      RowBox[{"f", ",", "\[Omega]1"}], "]"}], ",", "\[Omega]2"}], "]"}], "/.",
    "sublist"}], "/.", 
  RowBox[{"{", 
   RowBox[{"\[Delta]", "\[Rule]", 
    SuperscriptBox["10", 
     RowBox[{"-", "4"}]]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.691927557304541*^9, 3.6919275948308077`*^9}}],

Cell[BoxData[
 RowBox[{"0.00020970726940571296`", "\[VeryThinSpace]", "+", 
  RowBox[{"0.004562249017562428`", " ", "\[ImaginaryI]"}]}]], "Output",
 CellChangeTimes->{{3.691927572176893*^9, 3.691927595713313*^9}, 
   3.691929056386766*^9, 3.6919292442672167`*^9, 3.6919405108399487`*^9}]
}, Open  ]],

Cell["Test B:", "Text",
 CellChangeTimes->{{3.691927514764608*^9, 3.691927517015362*^9}, 
   3.6919292611899757`*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"sublist", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"\[Omega]", "\[Rule]", 
      RowBox[{"1.1", "+", 
       RowBox[{"I", "*", "0.8"}]}]}], ",", 
     RowBox[{
      RowBox[{"el", "[", "2", "]"}], "\[Rule]", "2.2"}], ",", 
     RowBox[{
      RowBox[{"el", "[", "4", "]"}], "\[Rule]", "4.4"}], ",", 
     RowBox[{
      RowBox[{"el", "[", "5", "]"}], "\[Rule]", "5.5"}], ",", 
     RowBox[{
      RowBox[{"\[Omega]k", "[", "1", "]"}], "\[Rule]", "1.1"}], ",", 
     RowBox[{
      RowBox[{"\[Omega]k", "[", "3", "]"}], "\[Rule]", "3.3"}], ",", 
     RowBox[{"\[Delta]", "\[Rule]", "0.8"}]}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.691927397222089*^9, 3.691927452253333*^9}, {
  3.691927491812808*^9, 3.691927518806861*^9}, {3.691929221303973*^9, 
  3.69192922668655*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"f", "=", 
   RowBox[{
    FractionBox["1", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"2", "*", "\[Pi]"}], ")"}], "2"]], 
    RowBox[{"G0", "[", 
     RowBox[{
      RowBox[{"\[Omega]", "-", "\[Omega]1"}], ",", "2"}], "]"}], "*", 
    RowBox[{"G0", "[", 
     RowBox[{
      RowBox[{"\[Omega]", "-", "\[Omega]1", "-", "\[Omega]2"}], ",", "4"}], 
     "]"}], "*", 
    RowBox[{"G0", "[", 
     RowBox[{
      RowBox[{"\[Omega]", "-", "\[Omega]1"}], ",", "5"}], "]"}], "*", 
    RowBox[{"\[Chi]", "[", 
     RowBox[{"\[Omega]1", ",", "1"}], "]"}], "*", 
    RowBox[{"\[Chi]", "[", 
     RowBox[{"\[Omega]2", ",", "3"}], "]"}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.691927519787834*^9, 3.691927521479697*^9}, {
  3.691929270066884*^9, 3.691929270154698*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NIntegrate", "[", 
  RowBox[{
   RowBox[{"f", "/.", "sublist"}], ",", 
   RowBox[{"{", 
    RowBox[{"\[Omega]1", ",", 
     RowBox[{"-", "Infinity"}], ",", "Infinity"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\[Omega]2", ",", 
     RowBox[{"-", "Infinity"}], ",", "Infinity"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.6919273500777903`*^9, 3.691927393384145*^9}, {
   3.6919274596296577`*^9, 3.6919274654852123`*^9}, 3.691927513377625*^9, 
   3.691927545704673*^9, 3.691929239062677*^9}],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"NIntegrate", "::", "slwcon"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"Numerical integration converging too slowly; suspect one \
of the following: singularity, value of the integration is 0, highly \
oscillatory integrand, or WorkingPrecision too small. \\!\\(\\*ButtonBox[\\\"\
\[RightSkeleton]\\\", ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/message/NIntegrate/slwcon\\\", ButtonNote -> \
\\\"NIntegrate::slwcon\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{
  3.691929056007248*^9, {3.691929236133819*^9, 3.691929239396109*^9}, 
   3.6919292715989513`*^9, 3.6919405109456673`*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "0.0003887714286282669`"}], "+", 
  RowBox[{"0.0061259942505134685`", " ", "\[ImaginaryI]"}]}]], "Output",
 CellChangeTimes->{
  3.691927363618849*^9, 3.691927393756221*^9, 3.6919274657785378`*^9, 
   3.6919275000046597`*^9, 3.691927546356277*^9, 3.691929056125355*^9, {
   3.691929237237672*^9, 3.6919292405006113`*^9}, 3.691929272598876*^9, 
   3.691940511953601*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"ResIntegrate", "[", 
    RowBox[{
     RowBox[{"ResIntegrate", "[", 
      RowBox[{"f", ",", "\[Omega]1"}], "]"}], ",", "\[Omega]2"}], "]"}], "/.",
    "sublist"}], "/.", 
  RowBox[{"{", 
   RowBox[{"\[Delta]", "\[Rule]", 
    SuperscriptBox["10", 
     RowBox[{"-", "4"}]]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.691927557304541*^9, 3.6919275948308077`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "0.0003887713991008279`"}], "+", 
  RowBox[{"0.006125994296511818`", " ", "\[ImaginaryI]"}]}]], "Output",
 CellChangeTimes->{{3.691927572176893*^9, 3.691927595713313*^9}, 
   3.691929056386766*^9, 3.6919292442672167`*^9, 3.691929278466875*^9, 
   3.691940512206292*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{
      FractionBox["1", 
       RowBox[{"2", "*", "\[Pi]"}]], 
      RowBox[{"ResIntegrate", "[", 
       RowBox[{
        RowBox[{
         FractionBox["1", 
          RowBox[{"2", "*", "\[Pi]"}]], 
         RowBox[{"ResIntegrate", "[", 
          RowBox[{
           RowBox[{
            RowBox[{"G0", "[", 
             RowBox[{
              RowBox[{"\[Omega]", "-", "\[Omega]1"}], ",", "2"}], "]"}], "*", 
            
            RowBox[{"G0", "[", 
             RowBox[{
              RowBox[{"\[Omega]", "-", "\[Omega]1", "-", "\[Omega]2"}], ",", 
              "4"}], "]"}], "*", 
            RowBox[{"G0", "[", 
             RowBox[{
              RowBox[{"\[Omega]", "-", "\[Omega]2"}], ",", "5"}], "]"}], "*", 
            
            RowBox[{"\[Chi]", "[", 
             RowBox[{"\[Omega]1", ",", "1"}], "]"}], "*", 
            RowBox[{"\[Chi]", "[", 
             RowBox[{"\[Omega]2", ",", "3"}], "]"}]}], ",", "\[Omega]1"}], 
          "]"}]}], ",", "\[Omega]2"}], "]"}]}], "/.", 
     RowBox[{"{", 
      RowBox[{"\[Delta]", "\[Rule]", "0"}], "}"}]}], "//", "Simplify"}], ",", 
   
   RowBox[{
    RowBox[{
     RowBox[{
      FractionBox["1", 
       RowBox[{"2", "*", "\[Pi]"}]], 
      RowBox[{"ResIntegrate", "[", 
       RowBox[{
        RowBox[{
         FractionBox["1", 
          RowBox[{"2", "*", "\[Pi]"}]], 
         RowBox[{"ResIntegrate", "[", 
          RowBox[{
           RowBox[{
            RowBox[{"G0", "[", 
             RowBox[{
              RowBox[{"\[Omega]", "-", "\[Omega]1"}], ",", "2"}], "]"}], "*", 
            
            RowBox[{"G0", "[", 
             RowBox[{
              RowBox[{"\[Omega]", "-", "\[Omega]1", "-", "\[Omega]2"}], ",", 
              "4"}], "]"}], "*", 
            RowBox[{"G0", "[", 
             RowBox[{
              RowBox[{"\[Omega]", "-", "\[Omega]1"}], ",", "5"}], "]"}], "*", 
            
            RowBox[{"\[Chi]", "[", 
             RowBox[{"\[Omega]1", ",", "1"}], "]"}], "*", 
            RowBox[{"\[Chi]", "[", 
             RowBox[{"\[Omega]2", ",", "3"}], "]"}]}], ",", "\[Omega]1"}], 
          "]"}]}], ",", "\[Omega]2"}], "]"}]}], "/.", 
     RowBox[{"{", 
      RowBox[{"\[Delta]", "\[Rule]", "0"}], "}"}]}], "//", "Simplify"}]}], 
  "}"}]], "Input",
 CellChangeTimes->{{3.691931647984498*^9, 3.691931675718302*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   FractionBox["1", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"\[Omega]", "-", 
       RowBox[{"el", "[", "2", "]"}], "-", 
       RowBox[{"\[Omega]k", "[", "1", "]"}]}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{"\[Omega]", "-", 
       RowBox[{"el", "[", "5", "]"}], "-", 
       RowBox[{"\[Omega]k", "[", "3", "]"}]}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "\[Omega]"}], "+", 
       RowBox[{"el", "[", "4", "]"}], "+", 
       RowBox[{"\[Omega]k", "[", "1", "]"}], "+", 
       RowBox[{"\[Omega]k", "[", "3", "]"}]}], ")"}]}]], ",", 
   FractionBox["1", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"\[Omega]", "-", 
       RowBox[{"el", "[", "2", "]"}], "-", 
       RowBox[{"\[Omega]k", "[", "1", "]"}]}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{"\[Omega]", "-", 
       RowBox[{"el", "[", "5", "]"}], "-", 
       RowBox[{"\[Omega]k", "[", "1", "]"}]}], ")"}], " ", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"-", "\[Omega]"}], "+", 
       RowBox[{"el", "[", "4", "]"}], "+", 
       RowBox[{"\[Omega]k", "[", "1", "]"}], "+", 
       RowBox[{"\[Omega]k", "[", "3", "]"}]}], ")"}]}]]}], "}"}]], "Output",
 CellChangeTimes->{{3.691931660001059*^9, 3.691931676315823*^9}, 
   3.691940512465436*^9}]
}, Open  ]]
},
WindowSize->{808, 688},
WindowMargins->{{Automatic, 217}, {Automatic, 34}},
FrontEndVersion->"10.2 for Mac OS X x86 (32-bit, 64-bit Kernel) (July 29, \
2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 351, 9, 51, "Input"],
Cell[912, 31, 364, 9, 51, "Input"],
Cell[1279, 42, 265, 4, 30, "Text"],
Cell[1547, 48, 809, 17, 49, "Input"],
Cell[2359, 67, 247, 5, 30, "Text"],
Cell[2609, 74, 316, 10, 35, "Input"],
Cell[2928, 86, 206, 4, 30, "Text"],
Cell[3137, 92, 514, 14, 28, "Input"],
Cell[3654, 108, 977, 26, 46, "Input"],
Cell[CellGroupData[{
Cell[4656, 138, 747, 19, 28, "Input"],
Cell[5406, 159, 590, 11, 50, "Output"]
}, Open  ]],
Cell[6011, 173, 150, 2, 30, "Text"],
Cell[6164, 177, 1705, 50, 125, "Input"],
Cell[CellGroupData[{
Cell[7894, 231, 556, 15, 49, "Input"],
Cell[8453, 248, 450, 9, 50, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8940, 262, 1187, 33, 145, "Input"],
Cell[10130, 297, 767, 19, 50, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10934, 321, 1134, 32, 145, "Input"],
Cell[12071, 355, 793, 20, 50, "Output"]
}, Open  ]],
Cell[12879, 378, 104, 1, 30, "Text"],
Cell[12986, 381, 317, 10, 49, "Input"],
Cell[13306, 393, 686, 16, 28, "Input"],
Cell[CellGroupData[{
Cell[14017, 413, 431, 9, 28, "Input"],
Cell[14451, 424, 429, 7, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[14917, 436, 240, 5, 28, "Input"],
Cell[15160, 443, 278, 5, 28, "Output"]
}, Open  ]],
Cell[15453, 451, 89, 1, 30, "Text"],
Cell[15545, 454, 823, 21, 46, "Input"],
Cell[16371, 477, 760, 22, 52, "Input"],
Cell[CellGroupData[{
Cell[17156, 503, 523, 12, 28, "Input"],
Cell[17682, 517, 653, 13, 60, "Message"],
Cell[18338, 532, 383, 6, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[18758, 543, 410, 12, 35, "Input"],
Cell[19171, 557, 287, 4, 28, "Output"]
}, Open  ]],
Cell[19473, 564, 117, 2, 30, "Text"],
Cell[19593, 568, 823, 21, 46, "Input"],
Cell[20419, 591, 809, 23, 52, "Input"],
Cell[CellGroupData[{
Cell[21253, 618, 523, 12, 28, "Input"],
Cell[21779, 632, 679, 13, 60, "Message"],
Cell[22461, 647, 408, 8, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[22906, 660, 410, 12, 35, "Input"],
Cell[23319, 674, 308, 6, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[23664, 685, 2393, 69, 279, "Input"],
Cell[26060, 756, 1297, 36, 90, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

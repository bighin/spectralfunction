/*
	spectral.c

	Spectral function for the angulon at first and second order of the diagrammatic expansion.
	Requires: GNU Scientific Library and (optionally) OpenMP support in the compiler.

	Giacomo Bighin, 2016-2017
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <complex.h>
#include <stdbool.h>

#ifndef M_PI
#define M_PI (3.14159265358979323846264338327950288)
#endif

#include "selfenergies.h"
#include "qpw.h"
#include "debug.h"

/*
	Tuning paramaters for numerical integration.
*/

size_t maxEval=1024*1024;
double relError=1e-5;

/*
	Epsilon, also defined in spectral.c

	This regulates the broadness of the peaks in the spectral function,
	they will be tending to a delta function when epsilon tends to zero.
	It is good then to keep epsilon reasonably small but not too small,
	so that the peaks are still visibile.

	The choice epsilon=0.0001 seems to be a good compromise.

	Also smaller values give rise to numerical instabilities for higher omegas
	in the second order self-energies.
*/

double epsilon=1e-2;

/*
	The epsilon to be used in the quasiparticle formula in qpw.c
*/

double qpw_epsilon=0.001;

/*
	Nice progress bar
*/

#include "libprogressbar/progressbar.h"


/*
	Angulon Green's function and spectral function, 1st order
*/

double complex Gang1(short L,double complex omega,double n)
{
	double complex one=1.0f;
	
	return one/(omega-L*(L+1)-Sigma1(L,omega,n));
}

double A1(short L,double omega,double n)
{
	return -(1.0f/M_PI)*cimag(Gang1(L,omega+I*epsilon,n));
}

/*
	Angulon Green's function and spectral function, using self-energy contributions up to the 2nd order.
*/

double complex Gang2(short L,double complex omega,double n)
{
	double complex one=1.0f;

	return one/(omega-L*(L+1)-Sigma1(L,omega,n)-Sigma2(L,omega,n));
}

double A2(short L,double omega,double n)
{
	return -(1.0f/M_PI)*cimag(Gang2(L,omega+I*epsilon,n));
}

int spectroscopic_signatures(double logn,bool secondorder,FILE *out)
{
	int milliomega,cnt=0;
	char banner[1024];

	progressbar *progress;

	for(milliomega=0;milliomega<5000;milliomega+=5)
		cnt++;

	snprintf(banner,1024,"Calculating spectroscopic signatures (logn=%f,L=1)...",logn);
	progress=progressbar_new(banner,cnt);

#pragma omp parallel for

	for(milliomega=0;milliomega<5000;milliomega+=5)
	{
		double omega=milliomega*0.001f;
		
		double complex localres1=Sigma1(1,omega,exp(logn));
		double complex localres2A=0.0f;
		double complex localres2B=0.0f;

		if(secondorder==true)
		{
			localres2A=Sigma2A(1,omega,exp(logn));
			localres2B=Sigma2B(1,omega,exp(logn));
		}

		double complex localres2=localres2A+localres2B;

#pragma omp critical

		{
			fprintf(out,"%f %f ",logn,omega);
			fprintf(out,"%f %f %f %f ",creal(localres1),cimag(localres1),creal(localres2),cimag(localres2));
			fprintf(out,"%f %f %f %f\n",creal(localres2A),cimag(localres2A),creal(localres2B),cimag(localres2B));
			fflush(out);

			progressbar_inc(progress);
		}
	}

	progressbar_finish(progress);

	return 0;
}

void spectral_function_1st(void)
{
	int centilogn;
	progressbar *progress;

	init_selection_rules();

	progress=progressbar_new("Calculating the spectral function...",(2000/2)*((16+1)*20));

#pragma omp parallel for

	for(centilogn=-1500;centilogn<500;centilogn+=2)
	{
		double omega;
		double logn=0.01f*centilogn;

		for(omega=-6;omega<=10;omega+=0.050f)
		{
			double localres;
			double complex localSigma1[3];

			localres=A1(0,omega,exp(logn))+A1(1,omega,exp(logn))+A1(2,omega,exp(logn));

			localSigma1[0]=Sigma1(0,omega,exp(logn));
			localSigma1[1]=Sigma1(1,omega,exp(logn));
			localSigma1[2]=Sigma1(2,omega,exp(logn));

#pragma omp critical

			{
				int j;
				
				printf("%f %f %f ",logn,omega,localres);
				
				for(j=0;j<=2;j++)
					printf("%f %f ",creal(localSigma1[j]),cimag(localSigma1[j]));

				printf("\n");
				
				fflush(stdout);

				progressbar_inc(progress);
			}
		}
	}

	progressbar_finish(progress);

	fini_selection_rules();
}

void spectral_function_2nd(short onlysigmas)
{
	int centilogn;
	progressbar *progress;

	init_selection_rules();

	progress=progressbar_new("Calculating the spectral function...",(2000/2)*((16+1)*20));

#pragma omp parallel for

	for(centilogn=-1500;centilogn<500;centilogn+=2)
	{
		double omega;
		double logn=0.01f*centilogn;

		for(omega=-6;omega<=10;omega+=0.050f)
		{
			double localres1,localres2;
			double complex localSigma1[3],localSigma2[3];

			if(onlysigmas==false)
			{
				localres1=A1(0,omega,exp(logn))+A1(1,omega,exp(logn))+A1(2,omega,exp(logn));
				localres2=A2(0,omega,exp(logn))+A2(1,omega,exp(logn))+A2(2,omega,exp(logn));
			}
			else
			{
				localres1=localres2=0.0f;
			}

			localSigma1[0]=Sigma1(0,omega,exp(logn));
			localSigma1[1]=Sigma1(1,omega,exp(logn));
			localSigma1[2]=Sigma1(2,omega,exp(logn));

			localSigma2[0]=Sigma2(0,omega,exp(logn));
			localSigma2[1]=Sigma2(1,omega,exp(logn));
			localSigma2[2]=Sigma2(2,omega,exp(logn));

#pragma omp critical

			{
				int j;

				printf("%f %f ",logn,omega);
				
				if(onlysigmas==false)
					printf("%f %f ",localres1,localres2);
				
				for(j=0;j<=2;j++)
					printf("%f %f ",creal(localSigma1[j]),cimag(localSigma1[j]));

				for(j=0;j<=2;j++)
					printf("%f %f ",creal(localSigma2[j]),cimag(localSigma2[j]));

				printf("\n");
				
				fflush(stdout);

				progressbar_inc(progress);
			}
		}
	}

	progressbar_finish(progress);

	fini_selection_rules();
}

short zeta1(short L,double logn,double *out)
{
	double localepsilon=0.001f;
	
	double omegalo,omegahi,omegamid,omega;
	double elo,ehi,emid;
	double dsigma;
	
	short c,maxiter=32;

	omegalo=-64.0f;
	omegahi=L*(L+1);

	elo=omegalo-L*(L+1)-creal(Sigma1(L,omegalo,exp(logn)));
	ehi=omegahi-L*(L+1)-creal(Sigma1(L,omegahi,exp(logn)));

	if((elo*ehi)>=0.0f)
	{
		fprintf(stderr,"Initial values do not bracket a root (elo=%f, ehi=%f).\n",elo,ehi);
		return -1;
	}

	for(c=0;c<maxiter;c++)
	{		
		omegamid=(omegahi+omegalo)/2.0f;
		emid=omegamid-L*(L+1)-creal(Sigma1(L,omegamid,exp(logn)));

		if(emid==0.0f)
			break;

		if((emid*elo)<=0.0f)
		{
			omegahi=omegamid;
			ehi=emid;
		}
	
		else if((emid*ehi)<=0.0f)
		{
			omegalo=omegamid;
			elo=emid;
		}
		
		else
		{
			fprintf(stderr,"Error in rootfinding algorithm.\n");
			return -1;
		}
	}

	omega=omegamid;

	dsigma=(Sigma1(L,omega+localepsilon,exp(logn))-Sigma1(L,omega,exp(logn)))/localepsilon;

	out[0]=1.0f/(1.0f-dsigma);
	out[1]=omega;
	
	return 0;
}

short zeta2(short L,double logn,double *zeta)
{
	double localepsilon=0.001f;
	
	double omegalo,omegahi,omegamid,omega;
	double elo,ehi,emid;
	double dsigma1,dsigma2;
	
	short c,maxiter=32;

	omegalo=-64.0f;
	omegahi=L*(L+1);

	elo=omegalo-L*(L+1)-creal(Sigma1(L,omegalo,exp(logn)))-creal(Sigma2(L,omegalo,exp(logn)));
	ehi=omegahi-L*(L+1)-creal(Sigma1(L,omegahi,exp(logn)))-creal(Sigma2(L,omegahi,exp(logn)));

	if((elo*ehi)>=0.0f)
	{
		fprintf(stderr,"Initial values do not bracket a root (elo=%f, ehi=%f).\n",elo,ehi);
		return -1;
	}

	for(c=0;c<maxiter;c++)
	{		
		omegamid=(omegahi+omegalo)/2.0f;
		emid=omegamid-L*(L+1)-creal(Sigma1(L,omegamid,exp(logn)))-creal(Sigma2(L,omegamid,exp(logn)));

		if(emid==0.0f)
			break;

		if((emid*elo)<=0.0f)
		{
			omegahi=omegamid;
			ehi=emid;
		}
	
		else if((emid*ehi)<=0.0f)
		{
			omegalo=omegamid;
			elo=emid;
		}
		
		else
		{
			fprintf(stderr,"Error in rootfinding algorithm.\n");
			return -1;
		}
	}

	omega=omegamid;

	dsigma1=(Sigma1(L,omega+localepsilon,exp(logn))-Sigma1(L,omega,exp(logn)))/localepsilon;
	dsigma2=(Sigma2(L,omega+localepsilon,exp(logn))-Sigma2(L,omega,exp(logn)))/localepsilon;

	zeta[0]=1.0f/(1.0f-dsigma1-dsigma2);
	zeta[1]=omega;
	
	return 0;
}

void usage(char **argv)
{
	printf("Usage: %s [debug_rules | debug_sigmas | spectral1st | spectral2nd | spectral1st_abb | signatures | qpw ] [parameters]\n",argv[0]);
}

int main(int argc,char **argv)
{
	if(argc<=1)
	{
		usage(argv);
		return 0;
	}

	if(!strcmp(argv[1],"debug_rules"))
	{
		init_selection_rules();
		debug_selection_rules();
		fini_selection_rules();
			
		return 0;
	}

	if(!strcmp(argv[1],"debug_sigmas"))
	{
		init_selection_rules();		
		debug_sigmas();
		fini_selection_rules();
			
		return 0;
	}

	if(!strcmp(argv[1],"spectral1st"))
	{
		spectral_function_1st();
			
		return 0;
	}

        if(!strcmp(argv[1],"spectral2nd"))
        {
                spectral_function_2nd(false);

                return 0;
        }

	if(!strcmp(argv[1],"spectral1st_abb"))
	{
		extern double abb;
	
		if(argc<=2)
		{
			printf("Usage: %s spectral1st_abb [abb]\n",argv[0]);
			exit(0);
		}

		abb=atof(argv[2]);
		spectral_function_1st();

		return 0;
	}

        if(!strcmp(argv[1],"spectral2nd_abb"))
        {
                extern double abb;

                if(argc<=2)
                {
                        printf("Usage: %s spectral2nd_abb [abb]\n",argv[0]);
                        exit(0);
                }

                abb=atof(argv[2]);
                spectral_function_2nd(false);

                return 0;
        }

        if(!strcmp(argv[1],"spectral2nd_abb_onlysigmas"))
        {
                extern double abb;

                if(argc<=2)
                {
                        printf("Usage: %s spectral2nd_abb [abb]\n",argv[0]);
                        exit(0);
                }

                abb=atof(argv[2]);

		maxEval=128*1024;
		relError=1e-4;

                spectral_function_2nd(true);

                return 0;
        }

	if(!strcmp(argv[1],"signatures"))
	{
		FILE *out;
		char fname[1024];
		char *prefix="sx1";
		bool secondorder=true;
		
		init_selection_rules();		

		snprintf(fname,1024,"data/%s.m85.dat",prefix);
		if((out=fopen(fname,"w+"))!=NULL)
		{
			spectroscopic_signatures(-8.5,secondorder,out);
			if(out)	fclose(out);
		}

		snprintf(fname,1024,"data/%s.m61.dat",prefix);
		if((out=fopen(fname,"w+"))!=NULL)
		{
			spectroscopic_signatures(-6.1,secondorder,out);
			if(out)	fclose(out);
		}

		snprintf(fname,1024,"data/%s.m45.dat",prefix);
		if((out=fopen(fname,"w+"))!=NULL)
		{
			spectroscopic_signatures(-4.5,secondorder,out);
			if(out)	fclose(out);
		}

		snprintf(fname,1024,"data/%s.m32.dat",prefix);
		if((out=fopen(fname,"w+"))!=NULL)
		{
			spectroscopic_signatures(-3.2,secondorder,out);
			if(out)	fclose(out);
		}

		fini_selection_rules();
			
		return 0;
	}

	if(!strcmp(argv[1],"signatures_ext"))
	{
		FILE *out;
		char fname[1024];
		char *prefix="sx1";
		bool secondorder=true;
		
		init_selection_rules();

		snprintf(fname,1024,"data/%s.m100.dat",prefix);
		if((out=fopen(fname,"w+"))!=NULL)
		{
			spectroscopic_signatures(-10.0,secondorder,out);
			if(out)	fclose(out);
		}

		snprintf(fname,1024,"data/%s.m120.dat",prefix);
		if((out=fopen(fname,"w+"))!=NULL)
		{
			spectroscopic_signatures(-12.0,secondorder,out);
			if(out)	fclose(out);
		}

		snprintf(fname,1024,"data/%s.m160.dat",prefix);
		if((out=fopen(fname,"w+"))!=NULL)
		{
			spectroscopic_signatures(-16.0,secondorder,out);
			if(out)	fclose(out);
		}

		snprintf(fname,1024,"data/%s.m10.dat",prefix);
		if((out=fopen(fname,"w+"))!=NULL)
		{
			spectroscopic_signatures(-1.0,secondorder,out);
			if(out)	fclose(out);
		}

		snprintf(fname,1024,"data/%s.10.dat",prefix);
		if((out=fopen(fname,"w+"))!=NULL)
		{
			spectroscopic_signatures(1.0,secondorder,out);
			if(out)	fclose(out);
		}

		snprintf(fname,1024,"data/%s.25.dat",prefix);
		if((out=fopen(fname,"w+"))!=NULL)
		{
			spectroscopic_signatures(2.5,secondorder,out);
			if(out)	fclose(out);
		}

		fini_selection_rules();
			
		return 0;
	}

	if(!strcmp(argv[1],"qpw"))
	{
		int milliomega,L;
		double logn=-5.0f;

		char banner[1024];
		progressbar *progress;

		init_selection_rules();

		snprintf(banner,1024,"Calculating Gang (logn=%f)...",logn);
		progress=progressbar_new(banner,(201*1000/100));

		for(milliomega=-1300*1000;milliomega<=100*1000;milliomega+=100)
		{
			double complex omega=0.001f*milliomega+I*epsilon;
			double complex gang1,gang2;

			printf("%f %f ",creal(omega),logn);

			for(L=0;L<=3;L++)
			{
				gang1=Gang1(L,omega,exp(logn));
				gang2=Gang2(L,omega,exp(logn));

				printf("%f %f %f %f ",creal(gang1),cimag(gang1),creal(gang2),cimag(gang2));
			}

			printf("\n");

			progressbar_inc(progress);
		}

		progressbar_finish(progress);

		fini_selection_rules();

		return 0;
	}

	if(!strcmp(argv[1],"zeta1"))
	{
		extern double abb;

		double out[2];
		short L,centilogn;

		if(argc<2)
		{
			printf("Usage: %s zeta [abb]\n",argv[0]);
			exit(0);
		}

		if(argc==3)
			abb=atof(argv[2]);

		for(centilogn=-1500;centilogn<500;centilogn+=10) //was: centilogn+=2
		{
			double logn=0.01f*centilogn;

			printf("%f ",logn);

			for(L=0;L<=2;L++)
			{
				zeta1(L,logn,out);
				printf("%f %f ",out[0],out[1]);
			}
			
			printf("\n");
		}

		return 0;
	}

	if(!strcmp(argv[1],"zeta2"))
	{
		extern double abb;
		short centilogn;

		if(argc<2)
		{
			printf("Usage: %s zeta [abb]\n",argv[0]);
			exit(0);
		}

		if(argc==3)
			abb=atof(argv[2]);

		init_selection_rules();

#pragma omp parallel for

		for(centilogn=-1500;centilogn<500;centilogn+=20) //was: centilogn+=2
		{
			double logn=0.01f*centilogn;
			double out1[6],out2[6];
			short L;

			for(L=0;L<=2;L++)
			{
				zeta1(L,logn,&out1[2*L]);
				zeta2(L,logn,&out2[2*L]);
			}
		
#pragma omp critical	
			{
				printf("%f ",logn);

				for(L=0;L<=2;L++)
					printf("%f %f ",out1[2*L],out1[2*L+1]);

				for(L=0;L<=2;L++)
					printf("%f %f ",out2[2*L],out2[2*L+1]);

				printf("\n");
			}
		}

		fini_selection_rules();

		return 0;
	}

	if(!strcmp(argv[1],"allzetas"))
	{
		extern double abb;
		short centiabb;

		if(argc<2)
		{
			printf("Usage: %s allzetas [abb]\n",argv[0]);
			exit(0);
		}

		init_selection_rules();

		/*
			We cannot parallelize the outer loop, as abb is an external variable.
		*/

		for(centiabb=10;centiabb<=10*100;centiabb+=10)
		{
			FILE *outfile;
			progressbar *progress;

			char outname[128],banner[128];
			short centilogn;

			abb=0.01*centiabb;

			snprintf(outname,128,"zeta.%f.dat",abb);
			snprintf(banner,128,"Calculating the QP weight (abb=%f)...",abb);

			outfile=fopen(outname,"w+");
			progress=progressbar_new(banner,100);

#pragma omp parallel for

			for(centilogn=-1500;centilogn<1000;centilogn+=20)
			{
				double logn=0.01f*centilogn;
				double out1[6],out2[6];
				short L;

				for(L=0;L<=2;L++)
				{
					zeta1(L,logn,&out1[2*L]);
					zeta2(L,logn,&out2[2*L]);
				}

#pragma omp critical
				{
					fprintf(outfile,"%f ",logn);

					for(L=0;L<=2;L++)
						fprintf(outfile,"%f %f ",out1[2*L],out1[2*L+1]);

					for(L=0;L<=2;L++)
						fprintf(outfile,"%f %f ",out2[2*L],out2[2*L+1]);

					fprintf(outfile,"\n");
				
					progressbar_inc(progress);
				}
			}

			progressbar_finish(progress);
			fclose(outfile);
		}

		fini_selection_rules();

		return 0;
	}

	if(!strcmp(argv[1],"qp_poles"))
	{
		double logn;

		double extern abb;
		
		abb=2.0f;

		for(logn=-15.0;logn<=5.0;logn+=0.1f)
		{
			double p0,p1,p2;
			
			p0=qp_pole(0,logn);
			p1=qp_pole(1,logn);
			p2=qp_pole(2,logn);
			
			printf("%f %f %f %f\n",logn,p0,p1,p2);
		}
		
		return 0;
	}

	if(!strcmp(argv[1],"qp_poles2"))
	{
		int centilogn;

		double extern abb;
		
		abb=2.0f;

		init_selection_rules();

#pragma omp parallel for

		for(centilogn=-1500;centilogn<=500;centilogn+=10)
		{
			double logn=0.01*centilogn;
			
			double p0,p1,p2;
			double q0,q1,q2;
			
			p0=qp_pole(0,logn);
			p1=qp_pole(1,logn);
			p2=qp_pole(2,logn);

			q0=qp_pole2(0,logn);
			q1=qp_pole2(1,logn);
			q2=qp_pole2(2,logn);

#pragma omp critical
			{
				printf("%f %f %f %f %f %f %f\n",logn,p0,p1,p2,q0,q1,q2);
				fflush(stdout);
			}
		}

		fini_selection_rules();
		
		return 0;
	}

	if(!strcmp(argv[1],"qp_test000"))
	{
		short L=1;
		double localepsilon=0.0001;
		double logn;

		init_selection_rules();

		for(logn=-15;logn<=5;logn+=0.1f)
		{
			double a1,b1,pole1;
			double a2,b2,pole2;

			pole1=qp_pole(L,logn);

			a1=Tau1(L,pole1+I*0.001,exp(logn));
			b1=-(Sigma1(L,pole1+I*0.001+localepsilon,exp(logn))-Sigma1(L,pole1+I*0.001,exp(logn)))/localepsilon;

			pole2=qp_pole(L,logn);

			a2=Tau2(L,pole2+I*0.001,exp(logn));
			b2=-(Sigma2(L,pole1+I*0.001+localepsilon,exp(logn))-Sigma2(L,pole1+I*0.001,exp(logn)))/localepsilon;

			printf("%f %f %f %f %f %f %f\n",logn,pole1,a1,b1,pole2,a2,b2);
			fflush(stdout);
		}

		fini_selection_rules();
		
		return 0;
	}

	if(!strcmp(argv[1],"qpw2"))
	{
		progressbar *progress;
		int centilogn;

		init_selection_rules();

		progress=progressbar_new("Quasiparticle weights, second order.",2000/10);

#pragma omp parallel for

		for(centilogn=-1500;centilogn<=500;centilogn+=10)
		{
			double logn=0.01*centilogn;

			double a1pole,b1pole,c1pole;
			double a2pole,b2pole,c2pole;

			double a1,b1,c1;
			double a2,b2,c2;

			a1pole=qp_pole(0,logn);
			b1pole=qp_pole(1,logn);
			c1pole=qp_pole(2,logn);

			a2pole=qp_pole2(0,logn);
			b2pole=qp_pole2(1,logn);
			c2pole=qp_pole2(2,logn);

			a1=zetaLM(0,logn,a1pole);
			b1=zetaLM(1,logn,b1pole);
			c1=zetaLM(2,logn,c1pole);

			a2=zeta2LM(0,logn,a2pole);
			b2=zeta2LM(1,logn,b2pole);
			c2=zeta2LM(2,logn,c2pole);

#pragma omp critical
			{
				printf("%f %f %f %f %f %f %f ",logn,a1,b1,c1,a2,b2,c2);
				printf("%f %f %f %f %f %f\n",a1pole,b1pole,c1pole,a2pole,b2pole,c2pole);
				fflush(stdout);

				progressbar_inc(progress);
			}
		}

		progressbar_finish(progress);

		fini_selection_rules();
		
		return 0;
	}

	if(!strcmp(argv[1],"qpw2epsilon"))
	{
		progressbar *progress;
		int centilogn;

		if(argc<3)
		{
			printf("Usage: %s qpw2epsilon [epsilon]\n",argv[0]);
			exit(0);
		}

		qpw_epsilon=atof(argv[2]);

		init_selection_rules();

		progress=progressbar_new("Quasiparticle weights, second order.",2000/10);

#pragma omp parallel for

		for(centilogn=-1500;centilogn<=500;centilogn+=10)
		{
			double logn=0.01*centilogn;

			double a1pole,b1pole,c1pole;
			double a2pole,b2pole,c2pole;

			double a1,b1,c1;
			double a2,b2,c2;

			a1pole=qp_pole(0,logn);
			b1pole=qp_pole(1,logn);
			c1pole=qp_pole(2,logn);

			a2pole=qp_pole2(0,logn);
			b2pole=qp_pole2(1,logn);
			c2pole=qp_pole2(2,logn);

			a1=zetaLM(0,logn,a1pole);
			b1=zetaLM(1,logn,b1pole);
			c1=zetaLM(2,logn,c1pole);

			a2=zeta2LM(0,logn,a2pole);
			b2=zeta2LM(1,logn,b2pole);
			c2=zeta2LM(2,logn,c2pole);

#pragma omp critical
			{
				printf("%f %f %f %f %f %f %f ",logn,a1,b1,c1,a2,b2,c2);
				printf("%f %f %f %f %f %f\n",a1pole,b1pole,c1pole,a2pole,b2pole,c2pole);
				fflush(stdout);

				progressbar_inc(progress);
			}
		}

		progressbar_finish(progress);

		fini_selection_rules();
		
		return 0;
	}

	if(!strcmp(argv[1],"qpwfull_rules"))
	{
		double beta2s[16];
		short lambdas[16];
		short js[16];

		short L;
				
		int c,cnt;
		double totalweight;

		if(argc<3)
		{
			printf("Usage: %s qpwfull_rules [L]\n",argv[0]);
			exit(0);
		}

		L=atoi(argv[2]);

		beta2(L,0.0f,0.0f,beta2s,lambdas,js,&cnt,&totalweight);

		for(c=0;c<cnt;c++)
			printf("%d lambda=%d j=%d\n",c,lambdas[c],js[c]);
	
		return 0;
	}

	if(!strcmp(argv[1],"qpwfull"))
	{
		int centilogn;
		short L;

		progressbar *progress;
		char banner[1024];

		if(argc<3)
		{
			printf("Usage: %s qpwfull [L]\n",argv[0]);
			exit(0);
		}

		L=atoi(argv[2]);

		snprintf(banner,1024,"Quasiparticle weights, L=%d",L);
		progress=progressbar_new(banner,320*200);

#pragma omp parallel for

		for(centilogn=-1500;centilogn<500;centilogn+=10)
		{
			double omega;
			double logn=0.01f*centilogn;

			for(omega=-6;omega<=10;omega+=0.050f)
			{
				double zeta;
				
				zeta=zetaLM(L,logn,omega);

				{
					double beta2s[16];
					short lambdas[16];
					short js[16];
					
					int c,cnt;
					double totalweight;

					beta2(L,logn,omega,beta2s,lambdas,js,&cnt,&totalweight);

#pragma omp critical

					{
						printf("%f %f ",logn,omega);
						printf("%f ",zeta);

						for(c=0;c<cnt;c++)
							printf("%f ",beta2s[c]);
					
						printf("%f\n",totalweight);
					
						progressbar_inc(progress);
					}
				}
			}
		}

		progressbar_finish(progress);

		return 0;
	}

	if(!strcmp(argv[1],"debug0"))
	{
		FILE *out;
		char fname[1024];
		bool secondorder=true;
		
		init_selection_rules();

		snprintf(fname,1024,"data/debug0.dat");
		if((out=fopen(fname,"w+"))!=NULL)
		{
			spectroscopic_signatures(-4.5,secondorder,out);
			if(out)	fclose(out);
		}

		fini_selection_rules();
			
		return 0;
	}

	if(!strcmp(argv[1],"debug2"))
	{
		bool secondorder=true;
		double extern abb;

		if(argc<=2)
		{
			printf("Usage: %s debug2 [abb]\n",argv[0]);
			exit(0);
		}
	
		abb=atof(argv[2]);

		init_selection_rules();

		spectroscopic_signatures(-4.5,secondorder,stdout);

		fini_selection_rules();
			
		return 0;
	}

	usage(argv);
	return 0;
}
